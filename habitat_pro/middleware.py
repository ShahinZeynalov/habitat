from urllib.parse import parse_qs

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.db import close_old_connections
from channels.auth import AuthMiddleware, AuthMiddlewareStack, UserLazyObject
from channels.db import database_sync_to_async
from channels.sessions import CookieMiddleware, SessionMiddleware
from knox.auth import TokenAuthentication
from rest_framework import HTTP_HEADER_ENCODING
User = get_user_model()


@database_sync_to_async
def get_user(scope):
    close_old_connections()
    query_string = parse_qs(scope['query_string'].decode())
    token = query_string.get('token')
    if not token:
        return AnonymousUser()
    try:
        knox_auth = TokenAuthentication()
        if isinstance(token[0], str):
            token = token[0].encode(HTTP_HEADER_ENCODING)
        user, auth_token = knox_auth.authenticate_credentials(token=token)
    except Exception as exception:
        print('-------- exception', exception)
        return AnonymousUser()
    if not user.is_active:
        return
    return user


class TokenAuthMiddleware(AuthMiddleware):
    async def resolve_scope(self, scope):
        scope['user']._wrapped = await get_user(scope)



def TokenAuthMiddlewareStack(inner):
    return CookieMiddleware(SessionMiddleware(TokenAuthMiddleware(inner)))

