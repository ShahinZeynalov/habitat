import json
import datetime
from operator import itemgetter
from django.contrib.auth import get_user_model
from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.utils import timezone
from notifications.signals import notify

User = get_user_model()


class NotificationConsumer(AsyncJsonWebsocketConsumer):
    
    async def connect(self):
        user = self.scope['user']
        if user.is_anonymous:
            print('-------- anonymous user')
            await self.close()
        else:
            group = await self._get_user_group(user)
            print('----------- group', group)
            self.room_name = self.scope['url_route']['kwargs']['user_id']
            self.room_group_name = f'user_id_{self.room_name}'
            if group == 'group_none':
                await self.close()
            else:
                print('user ---', group, 'connected to the notify group')
            await self.channel_layer.group_add(
                group=f'{group}',
                channel=self.channel_name
            )

            await self.channel_layer.group_add(
                group='notify',
                channel=self.channel_name
            )
            await self.accept()

    async def disconnect(self, code):
        user = self.scope['user']
        await self.channel_layer.group_discard('notify', self.channel_name)
        # await self.channel_layer.group_discard(self.room_group_name, self.channel_name)
        await super().disconnect(code)

    async def receive_json(self, content, **kwargs):
        print('--- receive json', content,'---', **kwargs)

    # async def redactor_notifications(self, event):
    #     instance = event['instance']
    #     print('-------', event)
    #     event['info'] = f"{instance.get('user')} yeni söz əlavə etdi."
    #     if await self._send_notification(event):
    #         print('=------------------yeaapppp')
    #         await self.send_json(event)

    async def common_notifications(self, event):
        print('-------------send notification to redactors')
        await self.send_json(event)


    @database_sync_to_async
    def _send_notification(self, event):
        return True
        # print('------- scope user ', self.scope['user'].categories.all())
        # print('------- event category  ', event['instance'].get('categories'))
        # return self.scope['user'].categories.filter(id__in=[event['instance'].get('category')]).exists()


    @database_sync_to_async
    def _get_user_group(self, user):
        print('------------')
        if user.groups.first():
            return user.groups.first().name
        return 'group_none'
