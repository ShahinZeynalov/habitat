from rest_framework import serializers
from notifications.models import Notification



class NotificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Notification
        fields=(
        'id', 'level', 'actor_object_id',
        'actor_content_type', 'verb', 'target_object_id',
        'target_content_type', 'description', 'timestamp', 
        )
        # depth=1