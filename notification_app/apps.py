from django.apps import AppConfig


class NotificationAppConfig(AppConfig):
    name = 'notification_app'

    def ready(self):
        from . import signals
