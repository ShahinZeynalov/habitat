from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, post_delete, pre_save, pre_delete
from django.dispatch import receiver
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from django.contrib.admin.models import LogEntry
from habitat_app.models import Word, WordEditSuggestion, WordHistory
from django.core import serializers
from habitat_app.serializer import WordSerializer
from habitat_app.serializers.edit_serializers import SuggestAcceptSerializer

from notifications.signals import notify
import json
User = get_user_model()


def send_notification_by_word(sender, instance, created, **kwargs):
    print('----------------send_notification_by_word')
    if instance.user.groups.first() and instance.user.groups.first().name != 'admin':
        if created:
            if not instance.categories:
                print('----signal instance has not category')
                admins = User.objects.filter(groups__name='admin')
                notify.send(instance, recipient=instance.user, verb='Yeni söz əlavə olundu.')
                print('------', admins)
                channel_layer = get_channel_layer()
                async_to_sync(channel_layer.group_send)(
                    'notify',
                    {
                        "type": 'common.notifications',
                        "event": "New word added without category",
                        "instance": WordSerializer(instance).data,
                    }
                )
            else:
                print('------------- word signal runned',sender , kwargs)
                print('------user', instance.user)
                translators = User.objects.filter(groups__name='translator', categories__in=instance.categories.all()).distinct()
                print('---------translators', translators)
                notify.send(sender=instance.user, recipient=translators, target=instance, verb='Yeni söz əlavə edildi.')
                channel_layer = get_channel_layer()
                async_to_sync(channel_layer.group_send)(
                    'notify',
                    {
                        "type": 'common.notifications',
                        "event": "New word added",
                        "instance": WordSerializer(instance).data,
                    }
                )
        else:
            pass
            
post_save.connect(send_notification_by_word, Word)


def send_notifications_by_word_edit_suggestion(sender, instance, created, **kwargs):
    if created:
        print('-------- word suggestion created', )
        # if instance.word.categories:
        #     translators = User.objects.filter(groups__name='translator', categories__in=instance.word.categories.all()).distinct()
        # else:
        #     translators = User.objects.filter(groups__name='not translator')
        # print('---------translators', translators)
        notify.send(sender=instance.user, recipient=instance.user, target=instance, verb='Sözə təklif əlavə edildi.')
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            'notify',
            {
                "type": 'common.notifications',
                "event": "New suggestion added",
                "instance": SuggestAcceptSerializer(instance).data,
            }
        )
    else:
        pass

post_save.connect(send_notifications_by_word_edit_suggestion, WordEditSuggestion)
