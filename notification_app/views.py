from django.shortcuts import render
from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
import json
from rest_framework.generics import (
    CreateAPIView, DestroyAPIView, ListAPIView,
     RetrieveAPIView, UpdateAPIView, RetrieveUpdateDestroyAPIView, 
)
from notifications.models import Notification
from .serializers import NotificationSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions

class NotificationTemplateView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        print(self.request.user)
        context = super().get_context_data(**kwargs)
        context["group_name"] = mark_safe(json.dumps(self.request.user.groups.first().name))
        return context
    


class NotificationListAPIView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()
    