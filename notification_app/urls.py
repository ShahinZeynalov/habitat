from django.urls import path
from .views import  NotificationTemplateView, NotificationListAPIView


urlpatterns = [
    path('',NotificationTemplateView.as_view()),
    path('list/',NotificationListAPIView.as_view()),

    ]
