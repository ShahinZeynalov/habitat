from rest_framework import serializers
from .models import Word, Category, References, CommentWord, CommentToComment


class WordSerializer(serializers.ModelSerializer):
    campbell = serializers.IntegerField()

    class Meta:
        model = Word
        fields = (
            'id', 'user', 'name_en', 'name_az', 'campbell', 'in_glossary', 'definition', 'reference',
            'categories', 'references',
            'status', 'page')
        read_only_fields = ('id', 'user')


class WordListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = (
            'id', 'user', 'name_en', 'name_az', 'campbell', 'in_glossary', 'definition', 'reference',
            'categories',
            'status')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name',)
        read_only_fields = ('id',)


class ReferencesSerializer(serializers.ModelSerializer):

    class Meta:
        model = References
        fields = ('id', 'writer_name', 'book_name', 'date', 'book_link')
        read_only_fields = ('id',)


class WordSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = (
            'name_en', 'name_az', 'definition',
        )


class WordCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentWord
        fields = (
            'id', 'word', 'text'
        )
        read_only_fields = ('id',)


class WordCommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentWord
        fields = (
            'id', 'word', 'text', 'user', 'create_date'
        )


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentWord
        fields = (
            'id', 'text'
        )
        read_only_fields = ('id',)


class CommentToCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentToComment
        fields = (
            'id', 'comment', 'text',
        )
        read_only_fields = ('id',)


class ToCommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentToComment
        fields = (
            'id', 'comment', 'user', 'text', 'create_date'
        )


class ToCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentWord
        fields = (
            'id', 'text'
        )
        read_only_fields = ('id',)
