from rest_framework import serializers
from ..models import WordEditSuggestion, WordHistory
from rest_framework.fields import CurrentUserDefault

class SuggestCreateSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    class Meta:
        model = WordEditSuggestion
        fields = ('user', 'word', 'name_az', 'reference')
        read_only_fields=('user',)

class SuggestUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = WordEditSuggestion
        fields = ('accepted', 'name_az', 'reference')
        read_only_fields=('name_az', 'reference')

class SuggestAcceptSerializer(serializers.ModelSerializer):
    class Meta:
        model = WordEditSuggestion
        fields = ('name_az', 'reference')


class WordHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = WordHistory
        fields = ['id', 'data', 'created_at', ]