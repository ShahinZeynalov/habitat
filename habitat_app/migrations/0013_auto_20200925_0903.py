# Generated by Django 3.1.1 on 2020-09-25 09:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('habitat_app', '0012_auto_20200925_0902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='edit',
            name='status',
            field=models.PositiveIntegerField(choices=[(1, 'accepted'), (2, 'not accepted')], default=0),
        ),
    ]
