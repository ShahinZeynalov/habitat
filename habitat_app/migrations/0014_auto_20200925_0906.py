# Generated by Django 3.1.1 on 2020-09-25 09:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('habitat_app', '0013_auto_20200925_0903'),
    ]

    operations = [
        migrations.AlterField(
            model_name='edit',
            name='status',
            field=models.PositiveIntegerField(choices=[('accepted', 'accepted'), ('not accepted', 'not accepted')], default=0),
        ),
        migrations.AlterField(
            model_name='word',
            name='status',
            field=models.CharField(choices=[('translated', 'translated'), ('Nnot_translated', 'not_translated'), ('Accepted', 'accepted')], max_length=30),
        ),
    ]
