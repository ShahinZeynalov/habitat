# Generated by Django 3.1.1 on 2020-09-24 21:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('habitat_app', '0012_auto_20200924_0025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='word',
            name='category',
            field=models.ManyToManyField(related_name='catr', to='habitat_app.Category'),
        ),
    ]
