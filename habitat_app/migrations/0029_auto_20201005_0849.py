# Generated by Django 3.1.1 on 2020-10-05 08:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('habitat_app', '0028_auto_20201005_0838'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='references',
            name='publication_date',
        ),
        migrations.AddField(
            model_name='references',
            name='date',
            field=models.CharField(default=True, max_length=400),
            preserve_default=False,
        ),
    ]
