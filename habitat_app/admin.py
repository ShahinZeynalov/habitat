from django.contrib import admin
from .models import Word, Category, WordEditSuggestion, WordHistory, References


admin.site.register(Word)
admin.site.register(Category)
admin.site.register(References)
admin.site.register(WordEditSuggestion)
admin.site.register(WordHistory)
