from django.db import models
from django.utils.translation import ugettext_lazy as _


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return f'{self.name}'


type_choice = (
    ("translated", "translated"),
    ("not_translated", "not_translated"),
    ("accepted", "accepted"),
)


class References(models.Model):
    writer_name = models.CharField(max_length=255)
    book_name = models.CharField(max_length=255)
    date = models.DateField(null=True, blank=True)
    book_link = models.CharField(max_length=400)

    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return f'{self.writer_name}'


class Word(models.Model):
    user = models.ForeignKey('base_user.MyUser', on_delete=models.CASCADE, related_name="user")
    name_en = models.CharField(max_length=255, null=True, blank=True)
    name_az = models.CharField(max_length=255, null=True, blank=True)
    campbell = models.PositiveIntegerField(null=True, blank=True)
    in_glossary = models.BooleanField(default=True)
    definition = models.CharField(max_length=255, null=True, blank=True)
    reference = models.CharField(max_length=255, null=True, blank=True)
    image = models.ImageField(upload_to='word_img/', null=True, blank=True)
    categories = models.ManyToManyField(Category, related_name='words')
    page = models.PositiveIntegerField(null=True, blank=True)

    references = models.ForeignKey(References, on_delete=models.CASCADE, related_name='references', null=True,
                                    blank=True)

    status = models.CharField(max_length=30, choices=type_choice)

    def __str__(self):
        return f'{self.name_en}'


    def __init__(self, *args, **kwargs):
        super(Word, self).__init__(*args, **kwargs)
        self._original_status = self.status



class WordHistory(models.Model):
    word = models.ForeignKey('Word', on_delete=models.CASCADE)
    data = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)

    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.id} | {self.word.name_en}'


class WordEditSuggestion(models.Model):
    user = models.ForeignKey('base_user.MyUser', on_delete=models.CASCADE, related_name='edits')
    word = models.ForeignKey('Word', on_delete=models.CASCADE, related_name='suggests')
    name_az = models.CharField(max_length=255, null=True, blank=True)
    reference = models.CharField(max_length=1000, null=True, blank=True)
    accepted = models.BooleanField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name_az} {self.accepted}'


class CommentWord(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE, related_name='comment')
    user = models.ForeignKey('base_user.MyUser', on_delete=models.CASCADE, related_name='user_comment')
    text = models.CharField(max_length=255)
    create_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.word}'


class CommentToComment(models.Model):
    comment = models.ForeignKey(CommentWord, on_delete=models.CASCADE, related_name='comment_to_comment')
    user = models.ForeignKey('base_user.MyUser', on_delete=models.CASCADE, related_name='to_comment')
    text = models.CharField(max_length=255)
    create_date = models.DateTimeField(auto_now=True)
