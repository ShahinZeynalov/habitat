from django.urls import path
from .views import (
    WordDetail, WordList, CategoryList, CategoryCreate, 
    WordCreate, SuggestCreateAPIView, SuggestRUDAPIView,
    ExcelWord, ReferencesCreate, ReferencesList, WordReferences,
    WordHistoryListAPIView, search,WordCommentCreate,WordComments,CommentDetail,CommentToCommentCreate,
    ToComment,ToCommentDetail
)
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

urlpatterns = [
    path('search/', search, name='search'),
    path('word_create/',WordCreate.as_view()),
    path('word_edit_suggest/',SuggestCreateAPIView.as_view()),
    path('word_edit_suggest/<int:pk>',SuggestRUDAPIView.as_view()),
    path('word_history/',WordHistoryListAPIView.as_view()),
    path('word_list/',WordList.as_view()),
    path('references_list/',ReferencesList.as_view()),
    path('excel_word/',ExcelWord.as_view()),
    path('category_list/',CategoryList.as_view()),
    path('category_create/',CategoryCreate.as_view()),
    path('word/<int:pk>',WordDetail.as_view()),
    path('reference_word/<int:pk>',WordReferences.as_view()),
    path('reference_create/',ReferencesCreate.as_view()),
    path('word_comment/',WordCommentCreate.as_view()),
    path('word_comment/<int:pk>/',WordComments.as_view()),
    path('comment/<int:pk>/',CommentDetail.as_view()),
    path('comment_to_comment/',CommentToCommentCreate.as_view()),
    path('to_comment/<int:pk>',ToComment.as_view()),
    path('to_comment_update/<int:pk>',ToCommentDetail.as_view()),
    ]

urlpatterns += router.urls
