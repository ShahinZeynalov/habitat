import logging

from notifications.signals import notify
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from django.shortcuts import render
from django.http import HttpResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, viewsets, mixins, generics, permissions, filters
from rest_framework.generics import (
    ListAPIView, CreateAPIView, RetrieveUpdateAPIView, get_object_or_404,
    RetrieveUpdateDestroyAPIView, GenericAPIView
)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model

from base_user.models import MyUser
from habitat_app.serializers.edit_serializers import SuggestAcceptSerializer
from habitat_app.serializer import WordSerializer, WordListSerializer, CategorySerializer, ReferencesSerializer, \
    WordSearchSerializer, WordCommentSerializer, CommentSerializer, WordCommentsSerializer, CommentToCommentSerializer, \
    ToCommentsSerializer, ToCommentSerializer

from .serializers.edit_serializers import (
    SuggestCreateSerializer, SuggestUpdateSerializer, WordHistorySerializer
)
from .models import WordEditSuggestion, Word, Category, References, WordHistory, CommentWord, CommentToComment

from django.http import JsonResponse
from habitat_app.documents import WordDocument

User = get_user_model()


class WordCreate(CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = WordSerializer

    def create(self, request, *args, **kwargs):
        user_data = super().create(request, *args, **kwargs).data
        word = Word.objects.get(pk=user_data['id'])
        data = {
            'definition': word.definition,
            'reference': word.reference,
            'name_en': word.name_en,
            'name_az': word.name_az,
            'campbell': word.campbell,
            'in_glossary': word.in_glossary,
            'references': word.references.id,
            'status': word.status,

        }

        return Response(data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        print('------------', serializer)


class WordList(APIView):  # GET
    def get(self, request):
        words = Word.objects.all()
        serializer = WordListSerializer(words, many=True)
        return Response(serializer.data)


from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync


# For update
class WordDetail(APIView):
    permission_classes = [AllowAny]

    def get(self, request, pk):
        word = get_object_or_404(Word, pk=pk)
        print(word)
        data = WordSerializer(word).data
        return Response(data)

    @swagger_auto_schema(request_body=WordSerializer)
    def put(self, request, pk):  # update data
        a = self.request.user

        print(a)
        perm = a.groups.values_list('name', flat=True).last()
        print(perm)
        if perm == "editor" or perm == "translator":
            word = Word.objects.get(id=pk)
            if not word:
                return Response({"result": False, "message": "Word not Found"}, status=status.HTTP_404_NOT_FOUND)

            serializer = WordSerializer(instance=word, data=request.data, partial=True)

            if serializer.is_valid(raise_exception=True):
                notify.send(sender=self.request.user, recipient=self.request.user, target=word,
                            verb='Söz statusu dəyişildi.')
                channel_layer = get_channel_layer()
                print('------- user id in update', word.user.id)
                async_to_sync(channel_layer.group_send)(
                    'notify',
                    {
                        "type": 'common.notifications',
                        "event": f"Word  updated via notify group",
                        "instance": WordSerializer(word).data,
                    }
                )
                serializer.save()
                logging.info({"message": f"{word.name_en} has been Updated!"})

                return Response(serializer.data, status=status.HTTP_201_CREATED)

            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"result": False, "message": "Permissino Denied"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, pk):
        a = self.request.user

        print(a)
        perm = a.groups.values_list('name', flat=True).last()
        print(perm)
        if perm == "Idareci" or perm == "Tercumeci":
            word = get_object_or_404(Word, pk=pk)
            word.delete()
            logging.info(f'Deleted is Deleted')

            return Response({"message": f"{word.name_en} has been Deleted!"}, status=status.HTTP_200_OK)
        else:
            return Response({"result": False, "message": "Permissino Denied"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)


class CategoryCreate(CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CategorySerializer

    def create(self, request, *args, **kwargs):
        category_data = super().create(request, *args, **kwargs).data
        category = Category.objects.get(pk=category_data['id'])
        data = {
            'name': category.name,

        }

        return Response(data, status=status.HTTP_201_CREATED)


class CategoryList(APIView):  # GET
    def get(self, request):
        category = Category.objects.all()
        serializer = CategorySerializer(category, many=True)

        return Response(serializer.data)


import csv


class ExcelWord(APIView):
    def post(self, request):
        temp = {}
        category_temp = []
        file = request.FILES.get("file").read()

        file = file.decode("utf-8").splitlines()
        data = csv.DictReader(file)
        for i in data:
            temp = dict(i)
            # print(temp)
            if temp["in_glossary"] == "TRUE":
                temp['in_glossary'] = True

            elif temp["in_glossary"] == "FALSE":
                temp['in_glossary'] = False

            if temp['category'] != '':

                category_temp = temp['category'].split(',')
                # print(category_temp, '-------------')
                del temp['category']


            elif temp['category'] == '':
                del temp['category']

            if temp['user'] != '':
                user = MyUser.objects.filter(email=temp['user']).last()
                temp['user'] = user

            if temp['reference'] != '':
                a = temp['reference'].split(',')
                try:
                    obj = References.objects.get(writer_name=a[0], book_name=a[1], book_link=a[2],
                                                 date=a[-1])
                    temp['references'] = obj

                except References.DoesNotExist:
                    obj = References(writer_name=a[0], book_name=a[1], book_link=a[2],
                                     date=a[-1])
                    obj.save()
                temp['references'] = obj

                temp['name_en'] = temp.pop('english')
                temp['name_az'] = temp.pop('azerbaijani')
                temp['campbell'] = temp.pop('campbell_chapter')
                # temp['reference'] = temp.pop('reference')
                temp.pop('timestamp')
                # print(temp, 'tempp')

                word = Word.objects.create(**temp)

                for i in category_temp:
                    # print(i)
                    cat = Category.objects.filter(name=i).last()
                    # print(cat, '++++++++++++++')
                word.categories.add(cat)
        return Response({'result': True})


class ReferencesCreate(CreateAPIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = ReferencesSerializer


class ReferencesList(ListAPIView):  # GET
    queryset = References.objects.all()
    serializer_class = ReferencesSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['writer_name', 'book_name']


class WordReferences(APIView):
    def get(self, request, pk):
        word = Word.objects.filter(references_id=pk)
        serializer = WordSerializer(word, many=True)
        return Response(serializer.data)


class SuggestCreateAPIView(CreateAPIView):
    serializer_class = SuggestCreateSerializer
    permission_classes = (permissions.IsAuthenticated,)


class SuggestRUDAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = SuggestUpdateSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = WordEditSuggestion.objects.all()

    def perform_update(self, serializer):
        instance = self.get_object()
        verb = ''
        event = ''
        if serializer.validated_data.get('accepted'):
            word = Word.objects.filter(id=instance.word.id)
            if word:
                print('-------- word suggestion accepted', )
                serialized_word = SuggestAcceptSerializer(word[0]).data
                WordHistory.objects.create(word=word[0], data={**SuggestAcceptSerializer(instance).data})
                word.update(**{key: value for (key, value) in SuggestAcceptSerializer(instance).data.items() if value})
                verb = 'Söz təklifi qəbul edildi.'
                event = 'Sizin söz qəbul olundu.'
        elif not serializer.validated_data.get('accepted'):
            print('-------- word suggestion not accepted', )
            verb = 'Söz təklifi qəbul edilmədi.'
            event = 'Sizin söz qəbul olunmadı.'

        notify.send(sender=instance.user, recipient=instance.user, target=instance, verb=verb)
        channel_layer = get_channel_layer()
        print('------- user id', instance.user.id)
        async_to_sync(channel_layer.group_send)(
            f'notify',
            {
                "type": 'common.notifications',
                "event": event,
                "instance": SuggestAcceptSerializer(instance).data,
            }
        )
        WordEditSuggestion.delete(instance)


class WordHistoryListAPIView(ListAPIView):
    serializer_class = WordHistorySerializer
    filter_backends = [DjangoFilterBackend]
    queryset = WordHistory.objects.all()
    filterset_fields = ['word__id']


def search(request):
    q = request.GET.get('q')

    if q:
        words = WordDocument.search().query("multi_match", query=q, fields=["name_en", "name_az"])
        print(words)
    else:
        words = ''

    return JsonResponse(WordSearchSerializer(words, many=True).data, safe=False)


class WordCommentCreate(CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = WordCommentSerializer

    def create(self, request, *args, **kwargs):
        user_data = super().create(request, *args, **kwargs).data
        comment = CommentWord.objects.get(pk=user_data['id'])
        data = {
            'text': comment.text,
            'word': comment.word.id

        }

        return Response(data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        print('------------', serializer)


class WordComments(APIView):
    def get(self, request, pk):
        comment = CommentWord.objects.filter(word_id=pk)
        serializer = WordCommentsSerializer(comment, many=True)
        return Response(serializer.data)


class CommentDetail(APIView):
    permission_classes = [AllowAny]

    def get(self, request, pk):
        comment = get_object_or_404(CommentWord, pk=pk)
        print(comment)
        data = CommentSerializer(comment).data
        return Response(data)

    @swagger_auto_schema(request_body=CommentSerializer)
    def put(self, request, pk):  # update data
        a = self.request.user

        print(a)
        perm = a.groups.values_list('name', flat=True).last()
        print(perm)
        if perm == "admin" or perm == "translator":
            comment = CommentWord.objects.get(id=pk)
            if not comment:
                return Response({"result": False, "message": "Comment not Found"}, status=status.HTTP_404_NOT_FOUND)

            serializer = CommentSerializer(instance=comment, data=request.data, partial=True)

            if serializer.is_valid(raise_exception=True):
                notify.send(sender=self.request.user, recipient=self.request.user, target=comment,
                            verb='Comment dəyişildi.')
                channel_layer = get_channel_layer()
                print('------- user id in update', comment.user.id)
                async_to_sync(channel_layer.group_send)(
                    'notify',
                    {
                        "type": 'common.notifications',
                        "event": f"Comment  updated via notify group",
                        "instance": CommentSerializer(comment).data,
                    }
                )
                serializer.save()
                logging.info({"message": f"{comment.text} has been Updated!"})

                return Response(serializer.data, status=status.HTTP_201_CREATED)

            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"result": False, "message": "Permissino Denied"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, pk):
        a = self.request.user

        print(a)
        perm = a.groups.values_list('name', flat=True).last()
        print(perm)
        if perm == "admin" or perm == "translator":
            comment = get_object_or_404(CommentWord, pk=pk)
            comment.delete()
            logging.info(f'Comment is Deleted')

            return Response({"message": f"{comment.text} has been Deleted!"}, status=status.HTTP_200_OK)
        else:
            return Response({"result": False, "message": "Permissino Denied"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)


class CommentToCommentCreate(CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CommentToCommentSerializer

    def create(self, request, *args, **kwargs):
        user_data = super().create(request, *args, **kwargs).data
        comment = CommentToComment.objects.get(pk=user_data['id'])
        data = {
            'text': comment.text,

        }

        return Response(data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        print('------------', serializer)


class ToComment(APIView):
    def get(self, request, pk):
        comment = CommentToComment.objects.filter(id=pk)
        serializer = ToCommentsSerializer(comment, many=True)
        return Response(serializer.data)


class ToCommentDetail(APIView):
    permission_classes = [AllowAny]

    def get(self, request, pk):
        comment = get_object_or_404(CommentToComment, pk=pk)
        print(comment.user)
        data = ToCommentSerializer(comment).data
        return Response(data)

    @swagger_auto_schema(request_body=ToCommentSerializer)
    def put(self, request, pk):  # update data
        a = self.request.user
        b = get_object_or_404(CommentToComment, pk=pk)
        user = b.user

        print(a)
        perm = a.groups.values_list('name', flat=True).last()
        print(perm)
        if perm == "admin" or perm == "translator" or a == user:
            comment = CommentToComment.objects.get(id=pk)
            if not comment:
                return Response({"result": False, "message": "Comment not Found"}, status=status.HTTP_404_NOT_FOUND)

            serializer = ToCommentSerializer(instance=comment, data=request.data, partial=True)

            if serializer.is_valid(raise_exception=True):
                notify.send(sender=self.request.user, recipient=self.request.user, target=comment,
                            verb='Comment dəyişildi.')
                channel_layer = get_channel_layer()
                print('------- user id in update', comment.user.id)
                async_to_sync(channel_layer.group_send)(
                    'notify',
                    {
                        "type": 'common.notifications',
                        "event": f"Comment  updated via notify group",
                        "instance": ToCommentSerializer(comment).data,
                    }
                )
                serializer.save()
                logging.info({"message": f"{comment.text} has been Updated!"})

                return Response(serializer.data, status=status.HTTP_201_CREATED)

            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"result": False, "message": "Permissino Denied"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, pk):
        a = self.request.user
        b = get_object_or_404(CommentToComment, pk=pk)
        user = b.user

        print(a)
        perm = a.groups.values_list('name', flat=True).last()
        print(perm)
        if perm == "admin" or perm == "translator" or a == user:
            comment = get_object_or_404(CommentToComment, pk=pk)
            comment.delete()
            logging.info(f'Comment is Deleted')

            return Response({"message": f"{comment.text} has been Deleted!"}, status=status.HTTP_200_OK)
        else:
            return Response({"result": False, "message": "Permissino Denied"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)



