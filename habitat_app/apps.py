from django.apps import AppConfig


class HabitatAppConfig(AppConfig):
    name = 'habitat_app'
