from django.urls import path
from .views import *
from rest_framework.routers import DefaultRouter
from knox.views import LoginView
router = DefaultRouter()
urlpatterns = [
    path('login/', MyLoginView.as_view(), name="login"),
    path('registration/', UserRegistration.as_view(), name="registration"),
    path('group/', GroupListView.as_view(), name="group"),
    path('users/', UserListAPIView.as_view(), name="user_list"),
    path('user_delete/<int:pk>/', UserDestroyAPIView.as_view(), name="delete_user"),
    path('user_update/<int:pk>/', UserUpdateAPIView.as_view(), name="update_user"),
    path('user_retrieve/<int:pk>/', UserRetrieveAPIView.as_view(), name="retrieve_user"),
    ]

urlpatterns += router.urls
