from django.conf import settings
from django.contrib.auth.models import Group
from django.db.models import F
from drf_yasg.utils import swagger_auto_schema
from knox.models import AuthToken
from rest_framework.response import Response
from rest_framework import permissions, exceptions
from knox.views import LoginView
from rest_framework import status
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework.generics import (
    CreateAPIView, DestroyAPIView, ListAPIView,
     RetrieveAPIView, UpdateAPIView, RetrieveUpdateDestroyAPIView
) 
from rest_framework.views import APIView
from .serializer import *
from drf_yasg.utils import swagger_auto_schema
User = get_user_model()

class GroupListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = GroupApiSerializer
    queryset = Group.objects.all()

class MyLoginView(LoginView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = LoginUserSerializer

    @swagger_auto_schema(request_body=LoginUserSerializer)
    def post(self, request, format=None):
        serializer = LoginUserSerializer(data=request.data, context={'request': request})
        serializer.is_valid()
        if not serializer.is_valid(raise_exception=True):
            message = _("Unable to log in with provided credentials.")
            return Response({'message': message}, status=409)
        user = serializer.validated_data
        request.user = user
        token = super().post(request, format).data['token']
        response_data = {
            'email': user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'token': f"Token {token}"
        }

        # group = Group.objects.all().values(code=F("groupcustom__code"))
        # response_data.update({'group': group})
        return Response(response_data, status=status.HTTP_200_OK)


class UserRegistration(CreateAPIView):
    permission_classes = (permissions.IsAdminUser,)
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        user_data = super().create(request, *args, **kwargs).data
        user = User.objects.get(pk=user_data['id'])
        data = {
            'email': user.email,
            'first_name': user_data['first_name'],
            'last_name': user_data['last_name'],
        }
        return Response(data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            serializer = self.get_serializer(request.user)
            return Response(serializer.data, status=200)
        return Response(status=401)


class UserListAPIView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserListSerializer
    queryset = User.objects.all()


class UserDestroyAPIView(DestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()

class UserUpdateAPIView(UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer

class UserRetrieveAPIView(RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer
