from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from django.apps import apps


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('delete_all', nargs='?', default=False, type=bool)

    def handle(self, *args, **options):
        Group.objects.all().delete()
        print('Deleted all group.............')
        print('Creating started .............')
        roles = [
            {"name": "redactor",  "permissions": ['habitat_app.Word'], "code_keyword":"elmi_redaktor"},
            {"name": "translator", "permissions": ['habitat_app.Word'], "code_keyword":"tercumeci"},
            {"name": "admin", "permissions": ['base_user.MyUser', 'habitat_app.Word'], "code_keyword": "idareci"},

        ]
        for role in roles:
            name = role.get('name')
            code = role.get('code_keyword')
            group, _ = Group.objects.get_or_create(name=name)
            # GroupCustom.objects.get_or_create(group=group, code=code)
            permissions = role.get('permissions')
            for permission in permissions:
                app_label, model = permission.split(".")
                my_model = apps.get_model(app_label, model)
                permission_obj = Permission.objects.filter(content_type=(ContentType.objects.get_for_model(my_model)))
                for per in permission_obj:
                    group.permissions.add(per)
            print(group)
        print('Migration completed successfully')
        print("Migration statistics")
        print('-------------------')


select={'renamed_value': 'cryptic_value_name'}