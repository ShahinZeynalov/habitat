from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.validators import validate_email
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.relations import PrimaryKeyRelatedField
from base_user.models import *
from knox.models import AuthToken
from django.db import transaction
User = get_user_model()



class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ('id',)

    def validated_data(self, *args, **kwargs):
        print('---------------------')
	# 	if "data" in kwargs:
	# 		data = kwargs["data"]

	# 		# check if many is required
    #         if isinstance(data, list):
    #             kwargs["many"] = True

	# 	return super(YourModel, self).get_serializer(*args, **kwargs)	 

class LoginUserSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(request=self.context['request'], **data)
        print('----', user)
        if not (user and user.is_active):
            raise serializers.ValidationError(_("Unable to log in with provided credentials."))
        return user


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', "first_name", 'last_name', 'email', 'password',
        'categories', 'about','groups', 'profile_photo',
          )
        extra_kwargs = {
            'first_name': {'required': True, 'allow_blank': False},
            'last_name': {'required': True, 'allow_blank': False},
            'email': {'required': True, 'allow_blank': False},
            'categories': {'required': True},
           
        }
        
        read_only_fields = ('id', )


    
    @transaction.atomic
    def create(self, validated_data):
        categories = validated_data.get('categories')
        groups = validated_data.get('groups')

        print('-----categories', categories)
        validated_data.pop('categories')
        print('--------------------', groups)
        validated_data['is_staff'] = True
        groups = validated_data.pop('groups')
        user = User.objects.create_user(**validated_data)
        user.groups.add(groups[0])
        user.categories.add(*categories)
        return user
    



class UserListSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format='%d-%m-%Y %H:%M')

    class Meta:
        model = User
        fields = ('id', "first_name", 'last_name','email',
        'categories', 'groups', 'profile_photo', 'created_at', 'updated_at'
          )
        extra_kwargs = {
            'categories': {'required': False},
        }


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', "first_name", 'last_name', 'email',
        'categories', 'groups', 'about', 'profile_photo'
          )
        extra_kwargs = {
            'first_name': {'required': True, 'allow_blank': False},
            'last_name': {'required': True, 'allow_blank': False},
            'email': {'required': True, 'allow_blank': False},
            'categories': {'required': True},
            'groups': {'required': True,},
        }
        read_only_fields = ('id', )

class GroupApiSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Group
        fields = ('id','name')
