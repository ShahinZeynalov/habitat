# Generated by Django 3.1.1 on 2020-09-19 10:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base_user', '0010_merge_20200917_2228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='profile_photo',
            field=models.ImageField(blank=True, default='spongebob.jpg', null=True, upload_to='', verbose_name='profilephoto'),
        ),
    ]
