# Generated by Django 3.1.1 on 2020-09-19 17:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('habitat_app', '0006_auto_20200919_1718'),
        ('base_user', '0009_merge_20200917_1619'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='categories',
            field=models.ManyToManyField(related_name='users', to='habitat_app.Category'),
        ),
    ]
