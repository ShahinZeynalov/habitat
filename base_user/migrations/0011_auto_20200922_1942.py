# Generated by Django 3.1.1 on 2020-09-22 19:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base_user', '0010_auto_20200919_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='profile_photo',
            field=models.ImageField(blank=True, default='person.png', null=True, upload_to='', verbose_name='profilephoto'),
        ),
    ]
